/*
 * HushPad project
 * Licensed under AGPL license: http://www.gnu.org/licenses/agpl-3.0.en.html
 * (c) 2016 John Smith, Roman Pushkin
 */

import { strToBuf } from './util';

var crypto = window.crypto || window.msCrypto;
var subtle = !!crypto ? crypto.subtle || crypto.webkitSubtle : undefined;

function webcrypto_supported() {
    return !!subtle;
}

function pbkdf_webapi(password, salt, iter_count) {
    return new Promise(function (resolve, reject) {
        subtle.digest({name: "SHA-256"}, strToBuf(password)).then(function(result){
            subtle.importKey("raw", result, {name: "PBKDF2"}, false, ["deriveKey"]).then(function(key){
                subtle.deriveKey({name: "PBKDF2", salt: strToBuf(salt), iterations: iter_count, hash: {name: "SHA-1"}},
                        key,
                        {name: "AES-CTR", length: 256},
                        true,
                        ["encrypt", "decrypt"]
                        )
                    .then(function (key) {
                        subtle.exportKey("raw", key)
                            .then(function (keydata) {
                                resolve(keydata);
                            })
                            .catch(function (err) {
                                reject(err);
                            });
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
            .catch(function (err) {
                reject(err);
            });
        });
    });
}

function hmac_webapi(key, data) {
    return new Promise(function (resolve, reject) {
        subtle.importKey("raw", key, {name: "HMAC", hash: {name: "SHA-256"}}, false, ["sign", "verify"])
            .then(function (key) {
                subtle.sign({name: "HMAC"}, key, data)
                .then(function (h) {
                    resolve(new Uint8Array(h));
                })
                .catch (function (err) {
                    reject(err);
                });
            })
            .catch (function (err) {
                reject(err);
            });
    });
}

function aes_cbc_encrypt_webapi(key, iv, data) {
    return new Promise(function (resolve, reject) {
        subtle.importKey("raw", key, {name: "AES-CBC"}, false, ["encrypt", "decrypt"])
            .then(function (key) {
                subtle.encrypt({name: "AES-CBC", iv: iv}, key, data)
                    .then(function (encData) {
                        resolve(new Uint8Array(encData));
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
            .catch(function (err) {
                reject(err);
            });
    });
}

function aes_cbc_decrypt_webapi(key, iv, encData) {
    return new Promise(function (resolve, reject) {
        subtle.importKey("raw", key, {name: "AES-CBC"}, false, ["encrypt", "decrypt"])
            .then(function (key) {
                subtle.decrypt({name: "AES-CBC", iv: iv}, key, encData.buffer)
                    .then(function (data) {
                        resolve(new Uint8Array(data));
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
            .catch(function (err) {
                reject(err);
            });
    });
}

export { webcrypto_supported, pbkdf_webapi, hmac_webapi, aes_cbc_encrypt_webapi, aes_cbc_decrypt_webapi };
