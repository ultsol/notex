/*
 * HushPad project
 * Licensed under AGPL license: http://www.gnu.org/licenses/agpl-3.0.en.html
 * (c) 2016 John Smith, Roman Pushkin
 */

/* This does not work in MS Edge. Can be workarounded with polyfill. */
function strToBuf(str) {
    return new TextEncoder("utf-8").encode(str);
}

function bufToStr(buf) {
    return new TextDecoder("utf-8").decode(buf);
}


function equalBuffers(buf1, buf2)
{
    if (buf1.byteLength != buf2.byteLength)
        return false;
    var dv1 = new Int8Array(buf1);
    var dv2 = new Int8Array(buf2);
    for (var i = 0 ; i != buf1.byteLength ; i++)
        if (dv1[i] != dv2[i]) return false;
    return true;
}

function rand(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function replaceAt (s, index, character) {
    return s.substr(0, index) + character + s.substr(index + character.length);
}

function distortion(el, replace_text) {
    var orig_text = el.innerText;
    if (orig_text.length != replace_text.length) {
        console.log('dist error');
        return;
    }
    var restore = (function () {el.innerText = this;}).bind(el.innerText);
    function rad(el, n) {
        var y = rand(0, 256) & 7 ? replace_text : orig_text;
        el.innerText = replaceAt(el.innerText, n, y[n]);
    }
    function iter(m) {
        var n, d = rand(0, 256);
        var t = Math.round(d/(Math.log(m) + 1));
        n = rand(0, orig_text.length);
        rad(el, n);
        if (m != 0) {
            setTimeout(iter, t, --m);
        } else {
            restore();
        }
    }
    setTimeout(iter, 4, 100);
}

function genRandomBytes(n) {
    var arr = new Uint8Array(n);
    crypto.getRandomValues(arr);
    return arr;
}

var stdchars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

function genRandomString(n) {
    var clen = stdchars.length;
    var maxrb = 255 - (256 % clen);
    var r = genRandomBytes(n * 4);
    var rv = '';
    for (var i = 0; i < r.length; i++) {
        var c = r[i];
        if (c > maxrb)
            continue;
        rv += stdchars[c % clen];
        if (rv.length == n)
            break;
    }
    return rv;
}

export { strToBuf, bufToStr, equalBuffers, distortion, genRandomBytes, genRandomString };
