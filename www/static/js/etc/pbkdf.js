/*
usage:
    pbkdf(password, salt, iter_count).then(function (key) {
        console.log('key:' + JSON.stringify(key));
    });
*/

function pbkdf(password, salt, iter_count) {
    return new Promise(function (resolve, reject) {
        var w = new Worker('/static/js/etc/pbkdf_worker.js');
        w.onmessage = function (e) {
            e = e.data;
            switch (e.type) {
            case 'sync':
                w.postMessage({type:'calc', password: password, salt: salt,
                               iter_count: iter_count});
                break;
            case 'data':
                w.postMessage({type:'exit'});
                resolve(e.key);
                break;
            }
        };
    });
}
