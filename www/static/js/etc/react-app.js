/*
 * HushPad project
 * Licensed under AGPL license: http://www.gnu.org/licenses/agpl-3.0.en.html
 * (c) 2016 John Smith, Roman Pushkin
 */

import React from 'react';
import ReactDOM from 'react-dom';
import json_xhr from 'json-xhr';
import base64js from 'base64-js';
import lzstr from 'lz-string';
import { webcrypto_supported, pbkdf_webapi, hmac_webapi, aes_cbc_decrypt_webapi, aes_cbc_encrypt_webapi } from './webcryptoapi';
import { strToBuf, bufToStr, equalBuffers, distortion, genRandomString, genRandomBytes } from './util';

var MAX_NOTE_SIZE = 512 * 1024;

function f0(salt, password) {
    return '___salt___' + salt + '__password__' + password;
}

function genPassword() {
    return genRandomString(10);
}

function reportError(resolve, where) {
    return function (err) {
        console.log('ERROR ' + where + ' ' + err);
        resolve({status: 'error', where: where, error: err});
    };
}

function isEmptyUri(href) {
    var x = href.split('/');
    return x.length <= 3 || x[3] == '';
}

function watchLabel(label_el, password_el, on) {
    var fn_click = function (e) {
        if (label_el.innerHTML.indexOf('SHOW') != -1) {
            password_el.setAttribute('type', 'text');
            label_el.innerHTML = 'HIDE';
        } else {
            password_el.setAttribute('type', 'password');
            label_el.innerHTML = 'SHOW';
        }
        password_el.focus();
        // set the cursor to the right of <input>
        var len = password_el.value.length * 2;
        password_el.setSelectionRange(len, len);
    };
    if (on) {
        label_el.addEventListener('click', fn_click);
    } else {
        label_el.removeEventListener('click', fn_click);
    }
}

function adjustTextareaHeight(el, win_margin) {
    if (el.scrollHeight > window.innerHeight - win_margin) {
        el.style.height = 0;
        el.style.height = (window.innerHeight - win_margin) + 'px';
    }
}

function textareaAutoResize(textarea, win_margin) {
    textarea.addEventListener('keyup', function () {
        if (window.innerHeight - win_margin < this.scrollHeight) {
            this.style.overflow = 'auto';
        } else {
            this.style.overflow = 'hidden';
            this.style.height = 0;
            this.style.height = this.scrollHeight + 'px';
        }
    }, false);
}

var about_msg = 'about';

var CheckboxInput = React.createClass({
  getInitialState: function () {
    return {
        checked: this.props.checked || false
     };
  },
  render: function () {
    return (
        <label>
            <input name={this.props.name} type="checkbox"
              id={this.props.id}
              checked={this.state.checked}
              onChange={this.handleChange}
              />
            <span>{this.props.label}</span>
        </label>
    );
  },
  handleChange: function(e) {
      this.setState({checked: e.target.checked});
      if (this.props.onUpdate)
        (this.props.onUpdate)(e.target.checked);
  }
});

var NoteEdit = React.createClass({
    getInitialState: function () {
        console.log('NoteEdit.getInitialState props:' + JSON.stringify(this.props))
        return {
            deletable: true,
            waitingForKey: false,
            frozen: false,
            message: ''
        };
    },
    handleSendEncryptedTextResult: function (r) {
        console.log('handleSendEncryptedTextResult r:' + JSON.stringify(r));
        switch (r.status) {
        case 'ok':
            //TODO implement client-side redirection with JS/React
            var is_empty = isEmptyUri(window.location.href);
            window.location.href = r.redir_url;
            if (!is_empty)
                window.location.reload();
            break;
        case 'error_rate_limit_hit':
            renderErrorMessage('You have exceeded your limit of requests');
            break;
        case 'error_note_size':
            renderErrorMessage('Your message is too large');
            break;
        default:
            this.showErrorMessage('ERROR ' + JSON.stringify(r));
            break;
        }
    },
    sendEncryptedText: function(noteid, password, text, deletable, use_hash) {
        return new Promise(function (resolve) {
                    if (text.length > MAX_NOTE_SIZE)
                        resolve({status: 'error_note_size'});
                    var compressed_text = lzstr.compressToEncodedURIComponent(text);
                    var salt = base64js.fromByteArray(genRandomBytes(10));
                    var iv = genRandomBytes(16);
                    var iter_count = 100000;
                    this.setState({waitingForKey: true});
                    pbkdf_webapi(password, salt, iter_count).then(function (key) {
                        this.setState({waitingForKey: false});
                        console.log('key:' + JSON.stringify(new Int32Array(key)));
                        aes_cbc_encrypt_webapi(key, iv, strToBuf(compressed_text)).then(function (enctext) {
                            hmac_webapi(key, strToBuf(f0(salt, password))).then(function (hmacpass) {
                                hmac_webapi(key, enctext.buffer).then(function (hmacenctext) {
                                    var data = {
                                        'id': noteid,
                                        'salt': salt,
                                        'iter_count': iter_count,
                                        'hmac_password': base64js.fromByteArray(hmacpass),
                                        'hmac_enctext': base64js.fromByteArray(hmacenctext),
                                        'iv': base64js.fromByteArray(iv),
                                        'enctext': base64js.fromByteArray(enctext),
                                        'deletable': deletable
                                    };
                                    json_xhr('POST', '/api/hushpad/1/enctext', data).then(function (r) {
                                        console.log('POST enctext done:' + JSON.stringify(r));
                                        var url = '/' + noteid;
                                        if (use_hash)
                                            url += '#' + password;
                                        resolve({status: r.response['status'], redir_url: url});
                                    })
                                    .catch(function (r) {
                                        resolve({status: r.response['status']});
                                    });
                                })
                                .catch(reportError(resolve, 'hmac enctext'));
                            })
                            .catch(reportError(resolve, 'hmac pass'));
                        })
                        .catch(reportError(resolve, 'aes encrypt'));
                    }.bind(this));
        }.bind(this));
    },
    showErrorMessage: function (text) {
        this.setState({message: text});
    },
    onDeletableUpdate: function (deletable) {
        this.setState({deletable: deletable});
    },
    submit: function () {
        var text = this.refs.note_text.value;
        if (text === '') {
            this.refs.note_text.focus();
            this.showErrorMessage('The note cannot be empty');
            return;
        }
        this.showErrorMessage('');
        this.setState({frozen: true});
        var password = this.refs.password.value;
        var deletable = this.state.deletable;
        var have_pwd = password !== '';
        if (!have_pwd)
            password = genPassword();
        this.sendEncryptedText(this.props.noteid, password, text, deletable, !have_pwd)
            .then(this.handleSendEncryptedTextResult);
    },
    handleButtonKeyPress: function (e) {
        if (e.which === 13 || e.which === 32) {
            e.preventDefault();
            this.submit();
        }
    },
    handleClick: function (e) {
        e.preventDefault();
        this.submit();
    },
    handleKeyPress: function (e) {
        if (e.which === 13) {
            e.preventDefault();
            this.submit();
        }
    },
    handleCtrlEnterKey: function (e) {
        if (e.ctrlKey && e.which == 13) {
            e.preventDefault();
            this.submit();
        }
    },
    handleKeyUp: function (e) {
        var label = this.refs.label_password;
        if (this.refs.password.value != '') {
            label.style.display = 'inline-block';
        } else {
            label.style.display = 'none';
            label.innerHTML = 'SHOW';
            this.refs.password.setAttribute('type', 'password');
        }
    },
    componentDidMount: function () {
        if (this.props.text)
            this.refs.note_text.value =  this.props.text;
        this.refs.note_text.focus();
        watchLabel(this.refs.label_password, this.refs.password, true);
        textareaAutoResize(this.refs.note_text, 230);
        distortion(this.refs.header1, 'encr');
        distortion(this.refs.header2, 'ypt');
    },
    componentWillUnmount: function () {
        watchLabel(this.refs.label_password, this.refs.password, false);
    },
    render: function () {
        return (
            <div className="container">
                <div className="about-link">
                    <a href="/about">about</a>
                </div>
                <h1>
                    <a href="/">
                        <span ref="header1" className="header1">hush</span>
                        <span ref="header2" className="header2">pad</span>
                    </a>
                </h1>
                <div id="enter-note">
                    <textarea ref="note_text"
                        placeholder="Type or paste here, hit Ctrl+Enter to submit"
                        readOnly={this.state.frozen}
                        onKeyDown={this.handleCtrlEnterKey}
                        />
                    {/*this.state.waitingForKey ? <p>WAITING FOR KEY(FIXME)</p> : null*/}
                    <div className="passwords password-container">
                        <input type="password" name="password" ref="password"
                            placeholder="Password (optional)"
                            readOnly={this.state.frozen}
                            onKeyPress={this.handleKeyPress}
                            onKeyUp={this.handleKeyUp}
                            />
                        <span ref="label_password" className="label-password">SHOW</span>
                    </div>
                    {this.state.message != '' ? <span className="error">{this.state.message}</span> : null}
                    <div className="options">
                        <CheckboxInput name="removable" label="Removable note" id="note-deletable" checked={true} onUpdate={this.onDeletableUpdate}/>
                    </div>
                    <div id="submit-btn">
                        <button type="submit" onClick={this.handleClick} onKeyPress={this.handleButtonKeyPress} readOnly={this.state.frozen}>Save</button>
                    </div>
                </div>
            </div>
            );
    }
});

function deleteNote(noteid, delkey) {
    return new Promise(function (resolve) {
        json_xhr('DELETE', '/api/hushpad/1/enctext/' + encodeURIComponent(noteid) + '?delkey=' + encodeURIComponent(delkey), {})
            .then(function (r) {
                resolve(r.response);
            })
            .catch(function (r) {
                resolve(r.response);
            });
    });
}

var NoteView = React.createClass({
    handleDeleteClicked: function () {
        if (this.props.onDelete)
            (this.props.onDelete)();
    },
    componentDidMount: function () {
        adjustTextareaHeight(this.refs.note_text, 200);
        this.refs.note_text.focus();
    },
    render: function () {
        return (
            <div className="container">
                <div className="about-link">
                    <a href="/about">about</a>
                </div>
                <h1>
                    <a href="/">
                        <span className="header1">hush</span>
                        <span className="header2">pad</span>
                    </a>
                </h1>
                <div id="note-view">
                    <textarea ref="note_text"
                        value={this.props.text}
                        readOnly={true}
                        />
                    <p id="remove">
                    {this.props.deletable ?
                        <button id="remove-btn" onClick={this.handleDeleteClicked}>Remove now</button>
                    : null}
                    </p>
                </div>
            </div>
            );
    }
});

function renderErrorMessage(msg) {
    var el = document.getElementById('app');
    ReactDOM.unmountComponentAtNode(el);
    ReactDOM.render(<Message text={msg}/>, el);
}

function renderNotSupported() {
    var el = document.getElementById('app');
    ReactDOM.unmountComponentAtNode(el);
    ReactDOM.render(<NotSupportedView/>, el);
}

function renderNewNote(noteid) {
    if (!window.app_state.create)
        return;
    var el = document.getElementById('app');
    ReactDOM.render(
        <NoteEdit noteid={noteid}/>,
        el
    );
    return true;
}

function handlePassword(noteid, salt, password, iter_count) {
    return new Promise(function (resolve) {
        pbkdf_webapi(password, salt, iter_count).then(function (key) {
                console.log('key:' + JSON.stringify(new Int32Array(key)));
                hmac_webapi(key, strToBuf(f0(salt, password))).then(function (hmacpass) {
                        var arg = {
                            'hmac_password': base64js.fromByteArray(hmacpass)
                        };
                        json_xhr('GET', '/api/hushpad/1/enctext/' + encodeURIComponent(noteid), arg).then(function (r) {
                                console.log('GET enctext -> ' + JSON.stringify(r));
                                var a = r.response;
                                if (a['status'] == 'ok') {
                                    var iv = base64js.toByteArray(a['iv']);
                                    var hmacenctext_their = base64js.toByteArray(a['hmac_enctext']);
                                    var enctext = base64js.toByteArray(a['enctext']);
                                    // check HMAC of encrypted text
                                    hmac_webapi(key, enctext.buffer).then(function (hmacenctext) {
                                            if (equalBuffers(hmacenctext.buffer, hmacenctext_their.buffer)) {
                                                aes_cbc_decrypt_webapi(key, iv, enctext).then(function (decodedBuf) {
                                                        var text = bufToStr(decodedBuf);
                                                        if (a['revision'] == 2)
                                                            text = lzstr.decompressFromEncodedURIComponent(text);
                                                        resolve({status:'ok', text: text, delkey: a['delkey'],
                                                                time: a['time'], cur_time: a['cur_time']});
                                                    })
                                                    .catch(reportError(resolve, 'ERROR aes decrypt'));
                                            } else {
                                                resolve({status: 'error_integrity_check'});
                                            }
                                        })
                                        .catch(reportError(resolve, 'hmac enctext'));
                                } else {
                                    resolve({status: a['status']});
                                }
                            })
                            .catch(function (r) {
                                resolve({status: r.response['status']});
                            });
                    })
                    .catch(reportError(resolve, 'hmac pass'));
            })
            .catch(reportError(resolve, 'pbkdf'));
    });
}

var EnterPassword = React.createClass({
    getInitialState: function () {
        return {
            message: ''
        };
    },
    componentDidMount: function () {
            this.refs.password.focus();
            watchLabel(this.refs.label_password, this.refs.password, true);
    },
    componentWillUnmount: function () {
            watchLabel(this.refs.label_password, this.refs.password, false);
    },
    submit: function () {
        var password = this.refs.password.value;
        if (password === '') {
            this.refs.password.focus();
            this.setState({message: 'The password cannot be empty'});
            return;
        }
        var noteid = this.props.noteid;
        var salt = this.props.salt;
        var iter_count = this.props.iter_count;
        handlePassword(noteid, salt, password, iter_count).then(function (r) {
                if (r.status == 'error_invalid_password') {
                    this.refs.password.focus();
                    this.setState({message: 'Invalid password'});
                } else {
                    (this.props.onResult)(r);
                }
            }.bind(this))
    },
    handleClick: function (e) {
        e.preventDefault();
        this.submit();
    },
    handleButtonKeyPress: function (e) {
        if (e.which === 13 || e.which === 32) {
            e.preventDefault();
            this.submit();
        }
    },
    handleKeyPress: function (e) {
        if (e.which === 13) {
            e.preventDefault();
            this.submit();
        }
    },
    handleKeyUp: function (e) {
        var label = this.refs.label_password;
        if (this.refs.password.value != '') {
            label.style.display = 'inline-block';
        } else {
            label.style.display = 'none';
            label.innerHTML = 'SHOW';
            this.refs.password.setAttribute('type', 'password');
        }
    },
    render: function () {
        // Do not handle form.onSubmit to avoid 'save password?' query of the browser
        // Handle key hits instead
        return (
            <div className="container">
                <h1>
                    <a href="/">
                        <span className="header1">hush</span>
                        <span className="header2">pad</span>
                    </a>
                </h1>
                <div id="unlock-view">
                <p>This note is password protected.</p>
                <div>
                    <p className="password-container">
                        <input type="password" name="password" ref="password"
                            placeholder="Type password here to unlock"
                            onKeyPress={this.handleKeyPress}
                            onKeyUp={this.handleKeyUp}
                            />
                        <span ref="label_password" className="label-password">SHOW</span>
                    </p>
                    {this.state.message != '' ? <p><span className="error">{this.state.message}</span></p> : null}
                    <p id="unlock-btn">
                        <button onClick={this.handleClick} onKeyPress={this.handleButtonKeyPress}>Unlock</button>
                    </p>
                </div>
                </div>
            </div>
            );
    }
});

var Message = React.createClass({
    render: function () {
        return (
            <div>
                <span>{this.props.text}</span>
            </div>
            );
    }
});

var MessageWithLink = React.createClass({
    render: function () {
        return (
            <div>
                <div id="deleted-view">
                    <span>{this.props.text}</span>
                    <a href={this.props.href}><button>{this.props.linktext}</button></a>
                </div>
            </div>
            );
    }
});

var NotSupportedView = React.createClass({
    componentDidMount: function () {
        distortion(this.refs.header1, 'encr');
        distortion(this.refs.header2, 'ypt');
    },
    render: function () {
        return (
            <div id="about-head">
                <h1>
                    <a href="/">
                        <span ref="header1" className="header1">hush</span><span ref="header2" className="header2">pad</span>
                    </a>
                </h1>
                <p>Sorry, seems your browser is missing the latest important <a href="http://caniuse.com/#search=web%20crypto">features</a>. Installing Chrome or Firefox should fix the issue!</p>
            </div>
            );
    }
});

function renderExistingEncryptedNote(el, noteid, salt, iter_count) {
    var on_result = function (r) {
        ReactDOM.unmountComponentAtNode(el);
        var on_delete = function () {
            deleteNote(noteid, r.delkey).then(function (r) {
                ReactDOM.unmountComponentAtNode(el);
                var msg;
                if (r.status == 'ok') {
                    ReactDOM.render(<MessageWithLink text='Note has been removed' linktext="New note" href="/"/>, el);
                    return;
                } else if (r.status == 'error_access_denied') {
                    msg = 'Error: access is denied';
                } else if (r.status == 'error_rate_limit_hit') {
                    msg = 'You have exceeded your limit of requests';
                } else if (r.status == 'error_not_found') {
                    msg = 'Error: no such note';
                } else {
                    msg = 'Error: ' + r.status;
                }
                ReactDOM.render(<Message text={msg}/>, el);
            });
        };
        if (r.status == 'ok') {
            var diff = Date.now() / 1000 - r.cur_time;
            ReactDOM.render(
                <NoteView noteid={noteid} text={r.text} deletable={!!r.delkey && r.delkey != ''}
                          time={r.time} diff={diff} onDelete={on_delete}/>,
                el);
        } else if (r.status == 'error_invalid_password') {
            ReactDOM.render(<Message text={'Invalid password'}/>, el);
        } else if (r.status == 'error_integrity_check') {
            ReactDOM.render(<Message text={'ERROR: integrity check failed'}/>, el);
        } else {
            ReactDOM.render(<Message text={'ERROR: ' + JSON.stringify(r)}/>, el);
        }
    };
    var password = window.location.hash.split('#')[1];
    if (password) {
        handlePassword(noteid, salt, password, iter_count).then(on_result);
    } else {
        ReactDOM.render(
            <EnterPassword noteid={noteid} salt={salt} iter_count={iter_count} onResult={on_result}/>,
            el
        );
    }
}

function renderExistingNote(noteid) {
    if (window.app_state.create)
        return;
    var el = document.getElementById('app');
    var noteid = window.app_state.noteid;
    var salt = window.app_state.salt;
    var iter_count = window.app_state.iter_count;
    renderExistingEncryptedNote(el, noteid, salt, iter_count);
    return true;
}

function isPromiseSupported() {
    return !!window.Promise;
}

function getIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");
    // If IE, return version number.
    if (Idx > 0) 
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    // If IE 11 then look for Updated user agent string.
    else if (!!navigator.userAgent.match(/Trident\/7\./)) 
        return 11;
    else
        return 0; //It is not IE
}

function isUnsupportedBrowser() {
    var agent = window.navigator.userAgent;
    // Safari is not supported (deriveKey is missing)
    if (agent.indexOf('Safari') >= 0 && agent.indexOf('Chrome') == -1)
        return true;
    // MS browsers (IE*, Edge) are unsupported due to improper Web Crypto API support
    if (agent.indexOf("MSIE") >= 0)
        return true;
    if (!!navigator.userAgent.match(/Trident\/7\./)) 
        return true;
    return (!!navigator.userAgent.match(/Edge\/\d+/));
}

function applyPolyfills() {
    if (!window.TextEncoder || !window.TextDecoder) {
        var T = require('text-encoding');
        window.TextEncoder = T.TextEncoder;
        window.TextDecoder = T.TextDecoder;
    }
}

if (!webcrypto_supported() || !isPromiseSupported() || isUnsupportedBrowser()) {
    renderNotSupported();
} else {
    applyPolyfills();
    var noteid = window.app_state.noteid;
    renderNewNote(noteid) || renderExistingNote(noteid);
}
