;
(function (root) {

'use strict';

var p = '/static/lib/sjcl/';
var window = {};
root.window = window;

importScripts(p + 'sjcl.js');

var hmacSHA1 = function (key) {
    var hasher = new sjcl.misc.hmac( key, sjcl.hash.sha1 );
    this.encrypt = function () {
        return hasher.encrypt.apply( hasher, arguments );
    };
};

onmessage = function (e) {
    e = e.data;
    switch (e.type) {
        case 'calc':
            {
                var key = sjcl.misc.pbkdf2(e.password, e.salt, e.iter_count, 256, hmacSHA1);
                postMessage({type:'data', key:key});
            }
            break;
        case 'exit':
            close();
            break;
    }
}

postMessage({type:'sync'});

}(this));