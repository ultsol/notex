NAME=notex

all:
	@gb build

test:
	@gb test

get-deps:
	@gb vendor restore

delete-deps:
	rm -rf ./vendor/src

run:
	bin/$(NAME) -debug -noratelimit

kill:
	@killall -TERM $(NAME) || true
	@killall gulp || true
	@killall scss || true

clean:
	find . -name '*.a' | xargs rm
	rm -f bin/$(NAME)

db-init:
	mysql --user="root" --password="foobar" < sql/tablestruct.sql

SCSS_DIR=www/static/scss
CSS_DIR=www/static/css

tools:
	npm i -g gulp
	go get github.com/constabulary/gb/...
	gem install --user-install bourbon neat bitters

init:
	npm i
	cd $(SCSS_DIR); bourbon install
	cd $(SCSS_DIR); neat install

gulp:
	gulp

gulp-watch:
	gulp watch

scss-watch:
	scss --watch $(SCSS_DIR):$(CSS_DIR)

scss:
	scss $(SCSS_DIR)/style.scss > $(CSS_DIR)/style.css
	scss $(SCSS_DIR)/design.scss > $(CSS_DIR)/design.css
	scss $(SCSS_DIR)/design-about.scss > $(CSS_DIR)/design-about.css

cert:
	box/gen-cert.sh

