var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var uglify = require('gulp-uglify');
var streamify = require('gulp-streamify');
var source = require('vinyl-source-stream');
var scss = require('gulp-scss');
var liveReload = require('gulp-livereload');

gulp.task('js', function () {
  browserify({
    basedir: 'www/static/js/etc/',
    entries: ['react-app.js', 'util.js', 'webcryptoapi.js'],
    extensions: ['.js'],
    debug: true
  })
  .transform(babelify, {presets: ['es2015', 'react']})
  .bundle()
  .pipe(source('bundle.js'))
  .pipe(streamify(uglify(/*{output: {ascii_only:true}}*/)))
  .pipe(gulp.dest('www/static/js/dist'));
});

//FIXME or REMOVE: this shit does not work at all: .css is not updated
gulp.task('scss', function () {
  gulp.src('www/static/scss/**/*.scss')
  .pipe(scss())
  .pipe(gulp.dest('www/static/css/'));
});

gulp.task('watch', function () {
  gulp.watch('www/static/js/etc/**/*.js', ['js']);
  //gulp.watch('www/static/scss/**/*.scss', ['scss']);
  var server = liveReload();
  gulp.watch(['www/static']).on('change', function (file) {
    server.changed(file.path);
  });
});

gulp.task('default', ['js', 'scss']);
