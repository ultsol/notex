- syntax highlight
    - place everything between <pre> and </pre>
    - if encounter ```LANG and ```LANG then place this in <code>...</code>
    - apply prismjs to this <code> element
+ remove autoremove of notes
+ remove auto-select
- replace notex->hushpad everywhere in src tree
+ hot code reload
+ limit size of message (512KB)
+ fix password generation in react-app (take from uniuri)
-+ Safari support
	+ drop off Safari for a while
	I deriveKey API is NOT supported in Safari
	---- some workaround?
- error-message-view
+ link '/' to '/about'
+ space between checkbox and "Removable note"
+ js: resizable TEXTAREA (on edit)
+ js: resizable TEXTAREA (on view)
+ about view
+ deleted-view
+ distortion in h1
+ js: put enctext on ctrl+enter in textarea
+ CI (separate project for buildbot)
- anti-robot
	+ rate limiting
	- rate limiting for specified IP addresses
		(feed them via files, use https://godoc.org/golang.org/x/exp/inotify for new IPs)
	---- 'recaptcha' if rate limit exceeded
	--- js monitoring of pressed keystrokes

- fill DB record fith zeros on removal

+ config (use TOML format)
+ js: button with eye at <input>
+ cut off old browsers (no Promise, no WebCryptoAPI)
+ js: fix 'save password?' issue
+ js: disable putting empty text with error "Please senter the text"
- web design:
    ---> RO -- color schme
+ get rid of /random API, generate random at client
+ js: quoted JSON fields for I/O with backend
- fill javascript array/string with zeros on recycling
- js: pbkdf2 and decrypt in WebWorker
+ vendor-specific prefixes for crypto api (ms, webkit)
- Edge support
- Safari support (at least Mac OS X)
- IE11 support???
- html/css: create sane design
	- only <textarea> is visible by default
	- <password>, <removable>, <send> are shown on
		- TAB pressed
		- mouse pointer is moved
	- line numbers in <textarea>: 
		I g: javascript textarea with line numbers
		I http://stackoverflow.com/questions/8431670/display-line-number-in-textarea
		I http://stackoverflow.com/questions/1995370/html-adding-line-numbers-to-textarea
	- try to use web-based text editor
		- ACE (https://ace.c9.io/#nav=about, http://github.com/ajaxorg/ace)
		- http://brackets.io/
		- ???
	- get rid of textarea in note view
		- select text as shown here: http://stackoverflow.com/questions/985272/selecting-text-in-an-element-akin-to-highlighting-with-your-mouse

	?- js: auto-resizing of textarea depending on text size
	?- js: select note text on right-click or Ctrl-A

	-second approach:
	- or rip the code from ace js editor

24 03 2016
+ kvs
+ on_http_req
+ move from tm
	+ npm
	+ sass, bourbon etc
	+ watch.sh

+ FIXME disable caching of /:note_id
+ BUG FIXME 'moved permanently' does not work!

+ FIXME GENERATE PASSWORD AFTER # ON CLIENT, NOT SERVER!!!

07 04 2016
+ DROP support for editable notes
---------- support for editable and readonly notes -- this is RISKY: data integrity can be harmed
---------	I note can be either editable or readonly
+ handle errors!

+ merge new_text and get_text templates

+ messages are ALWAYS encrypted
    + if password is empty THEN it will be generated and added after # to URL
    + if password is NOT empty THEN URL is without hash

+ do not redirect from '/' to '/noteid'

+ support of note deadline
	I 'Time until removal: 2 days 20:14:01' is shown in web UI for existing note
	I Deadline is updated each time message is edited

+ support for removal by user request
	+ 'delete' button is present on note view by default
	+ this button can be removed by unchecking 'delete button' checkbox on note creation

+ js client: extracting password from window.location.hash

+ middleware!
- CSRF
+ 	w.Header().Set("Content-Type", "text/html; charset=utf-8")
+ HSTS
	w.Header().Set("Strict-Transport-Security",
			"max-age=31536000; includeSubDomains")

