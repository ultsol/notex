var CheckboxInput = React.createClass({
  getInitialState: function () {
    return {
        checked: this.props.checked || false
     };
  },
  render: function () {
    return (
        <label>
            <input type="checkbox"
              name={this.props.name}
              checked={this.state.checked}
              readOnly={this.props.readonly}
              onClick={this.handleClick}
              />
              {this.props.label}
        </label>
    );
  },
  handleClick: function(e) {
      this.setState({checked: e.target.checked});
      (this.props.onUpdate)(e.target.checked);
  }
});


                        <CheckboxInput label="Read Only" name="ro"
                            readonly={this.state.frozen}
                            onUpdate={this.onReadonlyUpdate}/>



