#!/bin/bash

trap ctrl_c INT

function ctrl_c() {
  make kill
}

make gulp all
if [ $? != 0 ]
then
  echo Build failed
  return 1
fi
make gulp-watch &
make scss-watch &
sleep 1
make clean all run &
while inotifywait -r -e close_write ./www/tmpl/ ./src/ ; do
  killall -TERM notex
  make clean all
  if [ $? != 0 ]
  then
    echo Build failed
    return 1
  fi
  sleep 2
  make run &
done

