package ratelimit

import (
	"net/http"
	"time"

	"github.com/didip/tollbooth"
	"github.com/didip/tollbooth/config"
)

var limiters []*config.Limiter

func Check(w http.ResponseWriter, r *http.Request) (limited bool, msg string, status int) {
	limited = false
	return // FIXME rate limiting does not work well
	if len(limiters) == 0 {
		limited = false
		return
	}
	for _, limiter := range limiters {
		httpError := tollbooth.LimitByRequest(limiter, r)
		if httpError != nil {
			limited = true
			msg = httpError.Message
			status = httpError.StatusCode
			return
		}
	}
	limited = false
	return
}

func Init() {
	limiter := tollbooth.NewLimiter(10, time.Minute)

	// Configure list of places to look for IP address.
	// By default it's: "RemoteAddr", "X-Forwarded-For", "X-Real-IP"
	// If your application is behind a proxy, set "X-Forwarded-For" first.
	limiter.IPLookups = []string{"RemoteAddr", "X-Forwarded-For", "X-Real-IP"}
	limiter.Methods = []string{"POST", "DELETE"}
	limiters = append(limiters, limiter)

	limiter = tollbooth.NewLimiter(100, 24 * time.Hour)
	limiter.IPLookups = []string{"RemoteAddr", "X-Forwarded-For", "X-Real-IP"}
	limiter.Methods = []string{"POST", "DELETE"}
	limiters = append(limiters, limiter)
}
