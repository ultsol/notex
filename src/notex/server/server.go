package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"notex/config"
	"notex/data"
	"notex/ratelimit"
	"notex/util"
	"os"
	"path"
	"path/filepath"
	"strings"
	"syscall"
	"time"
	"crypto/tls"

	"github.com/dimfeld/httptreemux"
	"github.com/ultimatesolution/endless"
	"golang.org/x/crypto/acme/autocert"
)

var templates map[string]*template.Template = make(map[string]*template.Template)

func walkFn(path string, info os.FileInfo, err error) error {
	if info.IsDir() {
		return nil
	}
	filename := info.Name()
	// finename -> remove extension -> key
	extension := filepath.Ext(filename)
	name := filename[0 : len(filename)-len(extension)]
	templates[name] = template.Must(template.ParseFiles(path))
	return nil
}

func loadTemplates(path string) {
	err := filepath.Walk(path, walkFn)
	if err != nil {
		log.Fatalln("walk err:", err)
	}
}

func setHeaders(w http.ResponseWriter) {
	// UTF-8 content type
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	// Disable caching
	// taken here: http://stackoverflow.com/questions/49547/making-sure-a-web-page-is-not-cached-across-all-browsers
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")
}

func setHeadersMiddleware(fn httptreemux.HandlerFunc) httptreemux.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, p map[string]string) {
		setHeaders(w)
		fn(w, r, p)
	}
}

func hstsMiddleware(fn httptreemux.HandlerFunc) httptreemux.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, p map[string]string) {
		w.Header().Set("Strict-Transport-Security",
			"max-age=31536000; includeSubDomains")
		fn(w, r, p)
	}
}

func jsonResponse(w http.ResponseWriter, code int, response string) {
	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	fmt.Fprintf(w, "%s", response)
}

func jsonStatusResponse(w http.ResponseWriter, jsonStatus string) {
	j := &struct {
		Status string `json:"status"`
	}{
		jsonStatus,
	}
	buf, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	var code int
	if jsonStatus == "ok" {
		code = http.StatusOK
	} else if jsonStatus == "error_not_found" {
		code = http.StatusNotFound
	} else if jsonStatus == "error_internal" {
		code = http.StatusInternalServerError
	} else {
		code = http.StatusBadRequest
	}
	jsonResponse(w, code, string(buf))
}

func rateLimitAPIMiddleware(fn httptreemux.HandlerFunc) httptreemux.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, p map[string]string) {
		if limited, _, _ := ratelimit.Check(w, r); limited {
			jsonStatusResponse(w, "error_rate_limit_hit")
			return
		}
		fn(w, r, p)
	}
}

func errToStatus(err error) (status string) {
	if err != nil {
		return "error_internal"
	}
	return "ok"
}

func putEncryptedTextApi(w http.ResponseWriter, r *http.Request, _ map[string]string) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic("putEncryptedTextApi" + err.Error())
	}
	log.Println("putEncryptedTextApi\n", string(body)) //FIXME
	var j util.EncryptedNoteJson
	err = json.Unmarshal(body, &j)
	if err != nil {
		jsonStatusResponse(w, "error_invalid_request")
		return
	}
	if len(j.EncText) > config.C.MaxEncTextSize {
		jsonStatusResponse(w, "error_note_size")
		return
	}
	// does this node ID exist?
	_, err = util.GetNoteFromDb(j.Id)
	if err == nil {
		jsonStatusResponse(w, "error_collision")
		return
	}
	// put to DB
	err = util.PutEncryptedNoteToDb(&j)
	jsonStatusResponse(w, errToStatus(err))
}

func getEncryptedTextApi(w http.ResponseWriter, r *http.Request, p map[string]string) {
	var id string = p["id"]
	var hpass string = r.FormValue("hmac_password")
	log.Printf("getEncryptedTextApi id='%s'\n", id)
	log.Printf("getEncryptedTextApi hpass='%s'\n", hpass)
	note, err := util.GetNoteFromDb(id)
	if err != nil {
		jsonStatusResponse(w, "error_not_found")
		return
	}
	if hpass != note.HmacPassword {
		jsonStatusResponse(w, "error_invalid_password")
		return
	}
	j := &struct {
		Status      string `json:"status"`
		Revision    int    `json:"revision"`
		HmacEncText string `json:"hmac_enctext"`
		EncText     string `json:"enctext"`
		Iv          string `json:"iv"`
		DelKey      string `json:"delkey"`
		Time        int64  `json:"time"`
		CurTime     int64  `json:"cur_time"`
	}{
		"ok",
		note.Revision,
		note.HmacEncText,
		note.Text,
		note.Iv,
		note.DelKey,
		note.Time,
		time.Now().Unix(),
	}
	b, err := json.Marshal(j)
	if err != nil {
		panic("getEncryptedTextApi " + err.Error())
	}
	jsonResponse(w, http.StatusOK, string(b))
}

func deleteEncryptedTextApi(w http.ResponseWriter, r *http.Request, p map[string]string) {
	var id string = p["id"]
	var delkey string = r.FormValue("delkey")
	note, err := util.GetNoteFromDb(id)
	if err != nil {
		jsonStatusResponse(w, "error_not_found")
		return
	}
	// If the note is not deletable
	// or delkey is invalid
	if note.DelKey == "" || note.DelKey != delkey {
		jsonStatusResponse(w, "error_access_denied")
		return
	}
	err = util.DeleteNoteFromDb(id)
	jsonStatusResponse(w, errToStatus(err))
}

func renderNewNoteTemplate(w http.ResponseWriter, noteid string) {
	arg := struct {
		New    bool
		NoteId string
	}{
		true,
		noteid,
	}
	templates["index"].Execute(w, arg)
}

func renderError(err error, w http.ResponseWriter) {
	fmt.Fprintln(w, "ERROR:", err)
}

func renderExistingEncryptedNoteTemplate(w http.ResponseWriter, noteid string, note *data.Note) {
	arg := struct {
		New          bool
		NoteId       string
		Salt         string
		IterCount    int
	}{
		false,
		noteid,
		note.Salt,
		note.IterCount,
	}
	log.Printf("rendering encrypted note id='%s'\n", noteid)
	templates["index"].Execute(w, arg)
}

func httpRedirRequest(w http.ResponseWriter, r *http.Request, p map[string]string) {
	q := r.URL.Query()
	var url string
	for k, _ := range q {
		url = k
		break
	}
	http.Redirect(w, r, url, http.StatusSeeOther)
}

func staticFileRequest(w http.ResponseWriter, r *http.Request, p map[string]string) {
	fname := path.Base(r.URL.Path)
	http.ServeFile(w, r, "www/static/files/"+fname)
}

func aboutRequest(w http.ResponseWriter, r *http.Request, p map[string]string) {
	templates["about"].Execute(w, nil)
}

func newOrExistingNoteRequest(w http.ResponseWriter, r *http.Request, p map[string]string) {
	var noteid string = p["noteid"]
	if noteid == "" {
		// Create new note in "/" URL. Generate `noteid` here
		noteid = util.NewNoteId()
	}
	log.Println("noteid=", noteid)
	note, err := util.GetNoteFromDb(noteid)
	log.Printf("GetNoteFromDb -> (%+v, %+v)\n", note, err)
	if err != nil {
		renderNewNoteTemplate(w, noteid)
	} else {
		// Note	exists
		renderExistingEncryptedNoteTemplate(w, noteid, note)
	}
}

// RestrictedDir implements http.Dir with support of disabling directory listing.
// To disable directory listing in the browser just set EnableListing to false.
type RestrictedDir struct {
	Path          string
	EnableListing bool
}

func (d RestrictedDir) Open(name string) (http.File, error) {
	if filepath.Separator != '/' && strings.IndexRune(name, filepath.Separator) >= 0 ||
		strings.Contains(name, "\x00") {
		return nil, errors.New("http: invalid character in file path")
	}
	dir := string(d.Path)
	if dir == "" {
		dir = "."
	}
	fname := filepath.Join(dir, filepath.FromSlash(path.Clean("/"+name)))
	if !d.EnableListing {
		fi, err := os.Stat(fname)
		if err != nil {
			return nil, err
		}
		if fi.IsDir() {
			return nil, syscall.EPERM // return 'permission denied' error
		}
	}
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	return f, nil
}

func ServeFiles(r *httptreemux.TreeMux, path string, root http.FileSystem) {
	if len(path) < 10 || path[len(path)-10:] != "/*filepath" {
		panic("path must end with /*filepath in path '" + path + "'")
	}

	fileServer := http.FileServer(root)

	r.GET(path, func(w http.ResponseWriter, req *http.Request, ps map[string]string) {
		req.URL.Path = ps["filepath"]
		fileServer.ServeHTTP(w, req)
	})
}

func staticRouteSetup(r *httptreemux.TreeMux) {
	ServeFiles(r, "/static/*filepath", RestrictedDir{"www/static", false})
}

func redirHandler(w http.ResponseWriter, r *http.Request, p map[string]string) {
	redirToURL := config.C.TlsRedirUrl
	http.Redirect(w, r, redirToURL+r.RequestURI, http.StatusMovedPermanently)
}

func serveSingle(pattern string, filename string) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	})
}

func redir() {
	r := httptreemux.New()
	r.GET("/", redirHandler)
	r.GET("/*path", redirHandler)
	httpPort := util.PortToAddrStr(config.C.HttpPort)
	// try to listen until it succeeds
	for {
		http.ListenAndServe(httpPort, r)
		time.Sleep(10 * time.Second)
	}
}

func preSigHup() {
	log.Printf("Got SIGHUP signal: hot code reload\n")
}

func preSigInt() {
	log.Printf("Got SIGTERM signal\n")
	// server will be shut down in 10 sec
	endless.DefaultHammerTime = 10 * time.Second
	//TODO shutdown here
}

func Start() {
	loadTemplates("www/tmpl")

	r := httptreemux.New()
	p := "/api/hushpad/1"

	//FIXME favicon.ico handler
	r.GET("/favicon.ico", func(w http.ResponseWriter, r *http.Request, p map[string]string) {
		http.NotFound(w, r)
	})

	mws := []Middleware{setHeadersMiddleware/*, hstsMiddleware*/, rateLimitAPIMiddleware}

	// API
	r.POST(p+"/enctext", MW(putEncryptedTextApi, mws))
	r.GET(p+"/enctext/:id", MW(getEncryptedTextApi, mws))
	r.DELETE(p+"/enctext/:id", MW(deleteEncryptedTextApi, mws))

	// Web
	r.GET("/robots.txt", staticFileRequest)
	r.GET("/about", MW(aboutRequest, []Middleware{/*hstsMiddleware*/}))
	r.GET("/redir", MW(httpRedirRequest, []Middleware{/*hstsMiddleware*/}))
	r.GET("/", MW(newOrExistingNoteRequest, []Middleware{/*hstsMiddleware,*/ setHeadersMiddleware}))
	r.GET("/:noteid", MW(newOrExistingNoteRequest, []Middleware{/*hstsMiddleware,*/ setHeadersMiddleware}))

	staticRouteSetup(r)

	go redir()

	addr := util.PortToAddrStr(config.C.TlsPort)
	log.Println("Serving TLS on ", addr)
	endless.DefaultHammerTime = -1 // disable hammering the server
	srv := endless.NewServer(addr, r)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGHUP] = append(
		srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGHUP],
		preSigHup)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGINT] = append(
		srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGINT],
		preSigInt)
	var certFile, keyFile string
	if config.C.Domain != "" {
		// obtain TLS certificate from Let's Encrypt
		certManager := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(config.C.Domain),
			Cache:      autocert.DirCache("certs"), //folder for storing certificates
		}
		srv.TLSConfig = &tls.Config{
			GetCertificate: certManager.GetCertificate,
		}
		certFile = ""
		keyFile = ""
	} else {
		certFile = "tls/cert.pem"
		keyFile = "tls/key.pem"
	}
	err := srv.ListenAndServeTLS(certFile, keyFile)
	if err != nil {
		log.Println("ListenAndServeTLS -> error", err)
	}
	log.Println("Server has finished work")
}
