package server

import (
	"github.com/dimfeld/httptreemux"
)

type Middleware func(fn httptreemux.HandlerFunc) httptreemux.HandlerFunc

func MW(fn httptreemux.HandlerFunc, mws []Middleware) httptreemux.HandlerFunc {
	for i := len(mws) - 1; i >= 0; i-- {
		fn = (mws[i])(fn)
	}
	return fn
}
