package db

import (
	"errors"
	"log"

	"bytes"
	"compress/zlib"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
	"io/ioutil"
)

var (
	ErrorCollision = errors.New("DB collision by the key")
	ErrorNotFound  = errors.New("Not found")
)

type Kvs interface {
	Put(key string, data []byte) (err error)
	Get(key string) (data []byte, err error)
	Del(key string) (err error)
	Close()
}

type KvsEntry struct {
	Id      int64  `db:"id", primarykey, autoincrement`
	Key     string `db:"key" not null`
	Data    []byte `db:"data" not null`
	Deleted bool   `db:"deleted" not null`
}

type MysqlKvs struct {
	dbmap *gorp.DbMap
}

func NewMysqlKvs(dbmap *gorp.DbMap) (kvs *MysqlKvs, err error) {
	dbmap.AddTableWithName(KvsEntry{}, "kvs").SetKeys(true, "Id")
	kvs = &MysqlKvs{
		dbmap: dbmap,
	}
	return
}

func zlibCompress(data []byte) []byte {
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(data)
	w.Close()
	return b.Bytes()
}

func zlibDecompress(compressedData []byte) (data []byte, err error) {
	b := bytes.NewBuffer(compressedData)
	r, err := zlib.NewReader(b)
	defer r.Close()
	if err != nil {
		return
	}
	data, err = ioutil.ReadAll(r)
	return
}

func (kvs MysqlKvs) Put(key string, data []byte) (err error) {
	var update bool = false
	if _, err2 := kvs.Get(key); err2 == nil {
		update = true
	}
	encData := zlibCompress(data)
	entry := &KvsEntry{
		Id:      0,
		Key:     key,
		Data:    encData,
		Deleted: false,
	}
	if update {
		_, err = kvs.dbmap.Update(entry)
	} else {
		err = kvs.dbmap.Insert(entry)
	}
	if err != nil {
		log.Printf("mysql kvs Put (%+v, %v)\n", err, update)
	}
	return
}

func (kvs MysqlKvs) Get(key string) (data []byte, err error) {
	var entries []KvsEntry
	_, err = kvs.dbmap.Select(&entries,
		"select * from kvs where `key`=? and deleted=0", key)
	if err != nil {
		return
	}
	if len(entries) == 0 {
		err = ErrorNotFound
		return
	}
	if len(entries) > 1 {
		err = ErrorCollision
		return
	}
	var e *KvsEntry = &entries[0]
	data, err = zlibDecompress(e.Data)
	if err != nil {
		log.Println("mysql kvs Gut", err)
	}
	return
}

func (kvs MysqlKvs) Del(key string) (err error) {
	var entries []KvsEntry
	_, err = kvs.dbmap.Select(&entries,
		"select * from kvs where `key`=? and deleted=0", key)
	if err != nil {
		return
	}
	if len(entries) == 0 {
		err = ErrorNotFound
		return
	}
	if len(entries) > 1 {
		err = ErrorCollision
		return
	}
	var e *KvsEntry = &entries[0]
	if e.Deleted {
		return
	}
	e.Deleted = true
	_, err = kvs.dbmap.Update(e)
	return
}

func (kvs MysqlKvs) Close() {
}
