package db

import (
	"database/sql"
	"log"
	"notex/config"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
)

type JournalEntry struct {
	Id          int64  `db:"id", primarykey, autoincrement`
	Op          string `db:"op" not null`
	StorageType string `db:"storage_type" not null`
	Key         string `db:"key" not null`
	Time        int64  `db:"time"`
}

var kvs Kvs
var db *sql.DB
var dbmap *gorp.DbMap

func writeToJournal(key, storagetype, op string) (err error) {
	j := &JournalEntry{
		Id:          0,
		Op:          op,
		StorageType: storagetype,
		Key:         key,
		Time:        time.Now().Unix(),
	}
	err = dbmap.Insert(j)
	return
}

func Put(key string, data []byte) (err error) {
	kvs.Del(key)
	err = kvs.Put(key, data)
	if err != nil {
		log.Println("ERROR db.Put 1", err)
		return
	}
	err = writeToJournal(key, "my", "put")
	if err != nil {
		log.Println("ERROR db.Put 2", err)
		return
	}
	return
}

func Get(key string) (data []byte, err error) {
	data, err = kvs.Get(key)
	return
}

func Del(key string) (err error) {
	_, err = kvs.Get(key)
	if err != nil {
		return
	}
	kvs.Del(key)
	err = writeToJournal(key, "my", "del")
	if err != nil {
		log.Println("ERROR db.Del", err)
		return
	}
	return
}

// Remove obsolete KVS entries
func KillOld(deadlineSeconds int64) {
	entries, err := dbmap.Select(JournalEntry{},
		"select * from journal where time < ? and op=\"put\"",
		time.Now().Unix()-deadlineSeconds)
	if err != nil {
		log.Println("ERROR KillOld", err)
		return
	}
	for _, entry := range entries {
		j, ok := entry.(*JournalEntry)
		if !ok {
			log.Println("ERROR KillOld type cast")
		}
		if err := kvs.Del(j.Key); err != nil {
			log.Println("ERROR KillOld kvs.Del", err)
		}
		// Set "kil" (kill) token to avoid selecting this entry again
		j.Op = "kil"
		if _, err := dbmap.Update(j); err != nil {
			log.Println("ERROR KillOld update", err)
		}
	}
}

type logger struct{}

func (logger) Printf(format string, v ...interface{}) {
	log.Printf(format, v...)
}

func Init(debug bool) {
	var err error
	db, err = sql.Open("mysql", config.C.DbConnStr)
	if err != nil {
		log.Fatalln("DB open:", err)
	}
	dbmap = &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	if debug {
		dbmap.TraceOn("db:", logger{})
	}
	dbmap.AddTableWithName(JournalEntry{}, "journal").SetKeys(true, "Id")
	kvs, err = NewMysqlKvs(dbmap)
	if err != nil {
		log.Fatalln("Failed to create kvs", err)
	}
}

func Shutdown() {
	kvs.Close()
	db.Close()
}
