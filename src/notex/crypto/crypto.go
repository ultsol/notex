package crypto

import (
	"crypto/rand"
	"crypto/hmac"
	"crypto/sha256"
)

func GetRandomBytes(len int) (data []byte) {
	data = make([]byte, len)
	if _, err := rand.Read(data); err != nil {
		panic("GetRandomBytes error: " + err.Error())
	}
	return
}

func GetHmac(key, message []byte) (result []byte) {
	h := hmac.New(sha256.New, key)
	if _, err := h.Write(message); err != nil {
		panic("GetHmac error: " + err.Error())
	}
	result = h.Sum(nil)
	return
}
