package config

import (
	"github.com/BurntSushi/toml"
	"log"
)

type Config struct {
	HttpPort  int    `toml:"http_port"`
	TlsPort   int    `toml:"tls_port"`
	TlsRedirUrl  string `toml:"tls_redir_url"`
	DbConnStr string `toml:"db_conn_str"`
	// Note life time, seconds
	NoteLifetime int64 `toml:"note_lifetime"`
	MaxEncTextSize int `toml:"max_enc_text_size"`
	Domain    string   `toml:"domain"`
}

var C Config = Config{
	HttpPort:     2000,
	TlsPort:      20443,
	TlsRedirUrl: "https://hushpad.org",
	DbConnStr:    "root:foobar@tcp(localhost:3306)/notex",
	NoteLifetime: 3600 * 24 * 365,
	MaxEncTextSize: 1024 * 1024,
}

func Init(configFilePath string) {
	if _, err := toml.DecodeFile(configFilePath, &C); err != nil {
		log.Fatalln("Failed to load config: " + err.Error())
	}
	log.Printf("Loaded config: %+v\n", C)
}
