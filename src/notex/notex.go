package main

import (
	"flag"
	"log"
	"notex/config"
	"notex/db"
	"notex/server"
	"notex/ratelimit"
)

func main() {
	debug := flag.Bool("debug", false, "Turn on debug mode")
        noRateLimit := flag.Bool("noratelimit", false, "Turn off rate limiting")
	configFilePath := flag.String("config", "", "Configuration file name")
	flag.Parse()
	log.Printf("Notex (c) 2016\n")
	if *configFilePath != "" {
		config.Init(*configFilePath)
	}
	db.Init(*debug)
        if !*noRateLimit {
		ratelimit.Init()
        }
	//XXX not killing old notes ?
	//util.KillOldTask()
	defer db.Shutdown()
	server.Start()
}
