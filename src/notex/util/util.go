package util

import (
	"encoding/json"
	"github.com/dchest/uniuri"
	"log"
	"notex/config"
	"notex/data"
	"notex/db"
	"strconv"
	"time"
)

var forbiddenNoteIds = map[string]bool {
	"about": true,
	"robots.txt": true,
	"favicon.ico": true,
}

func NewNoteId() string {
	for {
		noteid := uniuri.NewLen(6)
		if _, ok := forbiddenNoteIds[noteid]; ok {
			continue
		}
		_, err := GetNoteFromDb(noteid)
		if err != nil { //FIXME if err == NOTFOUND_ERROR
			return noteid
		}
	}
}

func GetKey(noteid string) string {
	return noteid
}

func DeleteNoteFromDb(noteid string) error {
	return db.Del(GetKey(noteid))
}

func GetNoteFromDb(noteid string) (note *data.Note, err error) {
	buf, err := db.Get(GetKey(noteid))
	if err != nil {
		return
	}
	var n data.Note
	err = json.Unmarshal(buf, &n)
	if err != nil {
		log.Println("GetNodeFromDb ERROR unmarshal", err)
		return
	}
	note = &n
	return
}

type EncryptedNoteJson struct {
	Id        string `json:"id"`
	Salt      string `json:"salt"`
	IterCount int    `json:"iter_count"`
	HPass     string `json:"hmac_password"`
	HEncText  string `json:"hmac_enctext"`
	Iv        string `json:"iv"`
	EncText   string `json:"enctext"`
	Deletable bool   `json:"deletable"`
}

func PutEncryptedNoteToDb(j *EncryptedNoteJson) (err error) {
	var delkey string = ""
	if j.Deletable {
		delkey = uniuri.NewLen(20)
	}
	n := &data.Note{
		Revision:     data.NoteRevision,
		Salt:         j.Salt,
		IterCount:    j.IterCount,
		HmacPassword: j.HPass,
		HmacEncText:  j.HEncText,
		Iv:           j.Iv,
		Text:         j.EncText,
		DelKey:       delkey,
		Time:         time.Now().Unix(),
	}
	data, err := json.Marshal(n)
	if err != nil {
		panic(err)
	}
	err = db.Put(GetKey(j.Id), data)
	return
}

func GetNoteLifetime() int64 {
	return config.C.NoteLifetime
}

// Kill old notes periodically
func killOldTaskGoroutine() {
	for {
		time.Sleep(time.Second * 600)
		db.KillOld(GetNoteLifetime())
	}
}

func KillOldTask() {
	go killOldTaskGoroutine()
}

func PortToAddrStr(port int) string {
	return ":" + strconv.Itoa(port)
}
