package data

const NoteRevision = 2

type Note struct {
	Revision     int    `json:"r"`
	Salt         string `json:"salt"`
	IterCount    int    `json:"iter_count"`
	HmacPassword string `json:"hmac_password"` // base64-encoded
	HmacEncText  string `json:"hmac_enc_text"` // base64-encoded
	Iv           string `json:"iv"`            // base64-encoded
	Text         string `json:"text"`          // plain or encrypted text (base64-encoded)
	DelKey       string `json:"delkey"`
	Time         int64  `json:"time"`          // creation time
}
