#!/bin/bash

echo installing go &&\
pushd $HOME &&\
rm -rf ./go &&\
wget -nc https://storage.googleapis.com/golang/go1.5.1.linux-amd64.tar.gz &&\
tar xf go1.5.1.linux-amd64.tar.gz &&\
export GOROOT=$HOME/go &&\
export PATH=$PATH:$GOROOT/bin &&\
echo "export GOROOT=\$HOME/go" >> $HOME/.bashrc &&\
echo "export PATH=\$PATH:\$GOROOT/bin" >> $HOME/.bashrc &&\
echo installed go ok &&\
popd

