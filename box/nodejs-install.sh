#!/bin/bash

# WOrking installation of Node 0.12 to Ubuntu
# see https://nodesource.com/blog/nodejs-v012-iojs-and-the-nodesource-linux-repositories

echo installing node &&\
sh -c "curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash -" &&\
sudo apt-get install -y nodejs &&\
mkdir -p ~/.npm && echo 'prefix=~/.npm' > ~/.npmrc && echo 'export PATH=$HOME/.npm/bin:$PATH' >> ~/.bashrc &&\
source ~/.bashrc &&\
npm i -g npm &&\
echo installed node ok

