#!/bin/sh

mkdir -p ./tls
openssl genrsa -out ./tls/key.pem 2048 &&\
openssl req -new -x509 -key ./tls/key.pem -out ./tls/cert.pem -days 3650 &&\
echo certificates are generated ok

