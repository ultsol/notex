#!/bin/bash

curdir=`pwd` &&\
mkdir -p $HOME/sources/ &&\
cd $HOME/sources &&\
git clone https://github.com/nats-io/python-nats &&\
cd python-nats &&\
git checkout v0.2.2 &&\
sudo pip install -r requirements.txt &&\
sudo python2 setup.py install &&\
cd $curdir &&\
sudo pip install 'redis==2.10.5' &&\
sudo pip install 'python-telegram-bot==3.2' &&\
echo 'python libraries - installed ok'

