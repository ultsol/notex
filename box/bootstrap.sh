# might not need sudo here if you run this as privileged in Vagrantfile

if [ -z "$ROOT" ]; then
    export ROOT=$HOME/code/notex
fi

sudo apt-get update &&\
sudo apt-get install -y --fix-missing git libreadline6-dev libncurses-dev \
	build-essential make binutils autoconf automake \
	autotools-dev libtool pkg-config \
	inotify-tools ruby \
	python-pip python-setuptools &&\
sudo su -c 'gem install sass' &&\
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password foobar' &&\
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password foobar' &&\
sudo apt-get -y --force-yes install mysql-server mysql-client &&\
source $ROOT/box/golang-install.sh &&\
source $ROOT/box/nodejs-install.sh &&\
cp $ROOT/box/.screenrc $HOME/ &&\
export GOPATH=$HOME/gopath &&\
echo "export GOPATH=\$HOME/gopath" >> $HOME/.bashrc &&\
mkdir -p $GOPATH &&\
export PATH=$PATH:$GOPATH/bin &&\
echo "export PATH=\$PATH:\$GOPATH/bin" >> $HOME/.bashrc &&\
echo bootstrap ok

